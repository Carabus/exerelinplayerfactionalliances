# README #

Nexerelin is a mod for the game [Starsector](http://fractalsoftworks.com). It implements 4X-style gameplay with faction wars, diplomacy and planetary conquest.

Current release version: v0.5.2

### Setup instructions ###
Check out the repo to Starsector/mods/Exerelin (or some other folder name) and it can be played immediately. 

Create a project with Exerelin/jars/sources/ExerelinCore as a source folder to compile the Java files.